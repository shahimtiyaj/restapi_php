<?php
 
/*
 * Following code will get single product details
 * A product is identified by product id (pid)
 */
 
// array for JSON response
$response = array();
 
// include db connect class
include'db_connect.php';
 
// check for post data
//if (isset($_GET["pid"])) {
 //   $pid = $_GET['pid'];
 
    // get a product from products table
    $result = mysqli_query($conn,"SELECT *FROM products");
    $product = array();
    while($row = mysqli_fetch_array($result))
    {
            $pid= $row["pid"];
            $name = $row["name"];
            $price = $row["price"];
            $description = $row["description"];
            $created_at= $row["created_at"];
            $updated_at = $row["updated_at"];
            $ar=array("pid"=>$pid,"name"=>$name,"price"=>$price,"description"=>$description,"created_at"=>$created_at,"updated_at"=>$updated_at);
            $product[] = $ar;
    }
    $data = array("status"=>"success","data"=>$product);
    $json = json_encode($data);
    echo $json;
?>